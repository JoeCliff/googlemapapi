function initMap(){
    var options= {
        zoom:12,
        center:{lat:42.3601,lng:-71.0589}
    }
    var map = new
    google.maps.Map(document.getElementByID('map'),options);

    var marker = new google.maps.Marker({
        position:({lat:42.4668,lng:70.9495},
        map:map,
        icon:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
    });
    
}
